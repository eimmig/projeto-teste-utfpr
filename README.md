# Projeto Teste UTFPR

<h3> :man: &nbsp;Sobre mim </h3>

- 🎈 &nbsp; Projeto criado para a disciplina de Introdução a Engenharia da Computação.
- 🎓 &nbsp; Estudando Engenharia da Computação no <a href="http://www.utfpr.edu.br/">UTFPR-PB</a>.
- 🧱 &nbsp; Trabalhando como estagiário DEV na <a href="https://bitzsoftwares.com.br/">Bitz Softwares</a>

<h3> :rocket: &nbsp;Skills </h3>

- 🌎 &nbsp; Prestativo; </a>
- 🌎 &nbsp; Educado; </a>
- 🌎 &nbsp; Comunicativo; </a></a>

**Ferramentas de Desenvolvimento**

  ![Visual Studio Code](https://img.shields.io/badge/-Visual%20Studio%20Code-333333?style=flat&logo=visual-studio-code&logoColor=007ACC)
  
<h3> :earth_americas: &nbsp;Onde me encontrar: </h3> 

[![Linkedin: Eduardo Immig](https://img.shields.io/badge/-Linkedin-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/eduardoimmig/)]
[![Gmail Badge](https://img.shields.io/badge/-Gmail-006bed?style=flat-square&logo=Gmail&logoColor=white&link=mailto:eimmig@alunos.utfpr.edu.br)]
[![GitHub Eduardo Immig]( https://img.shields.io/github/followers/eimmig?label=follow&style=social)
